import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: any;
  cartItems: any;
  isUserLoggedIn: boolean;

  constructor(private http:HttpClient) {
this.cartItems=[];
this.loginStatus = new Subject();
  this.isUserLoggedIn = false;
  }
  
    //Login
    setIsUserLoggedIn() {
      this.isUserLoggedIn = true;
      this.loginStatus .next(true);
    }
  
    getIsUserLogged(): boolean {
      return this.isUserLoggedIn;
    }
    getLoginStatus():any{
      return this.loginStatus.asObservable();
    }
  
    //Logout
    setIsUserLoggedOut() {
      this.isUserLoggedIn = false;
      this.loginStatus.next(false);
    }
    addToCart(product:any){
      this.cartItems.push(product);
    }
    getCartItems(): any {
      return this.cartItems;
    }
  
    setCartItems() {
      this.cartItems.splice();
    }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  getAllEmployees(){
    return this.http.get('http://localhost:8085/getEmployees');
  }
  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }


  getEmployees(): any {
    return this.http.get('http://localhost:8085/getEmployees');
  }
  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }

  getDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }

  registerEmployee(emp: any): any {
    return this.http.post('http://localhost:8085/addEmployee', emp);
  }
  employeeLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
  }
  deleteEmployee(empId: any): any {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }
  updateEmployee(employee: any) {
    return this.http.put('http://localhost:8085/updateEmployee', employee);
  }




}