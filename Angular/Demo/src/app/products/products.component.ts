import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

interface Product {
  name: string;
  description: string;
  price: number;
  image: string;
}

interface CartItem {
  product: Product;
  quantity: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  
emailId: any;
products:any;
selectedProduct:any;
cartProducts: any;

  constructor(private service: EmpService) {

    this.emailId = localStorage.getItem('emailId');
    this.cartProducts=[];
    this.products = [
      // Example products
      {
        id:'101',
        name: 'HP-Laptop',
        description: 'HP Laptop 15s, AMD Ryzen 7 5700U, 15.6-inch (39.6 cm), FHD, 16GB DDR4, 512GB SSD',
        price: 19.99,
        image: 'https://www.netlabindia.com/wp-content/uploads/2019/05/HP-Laptop-PNG-Image.png'
      },
      { id:'102',
        name: 'Planter Pot',
        description: 'Hamdan Raised Planter Pot, Solid Acacia Wood Square Tabletop Plant Pot with Removable Modern Black Metal Stand with Wood Liner',
        price: 29.99,
        image: 'https://m.media-amazon.com/images/I/51Z2nOHFD1L._SX300_SY300_QL70_FMwebp_.jpg'
      },
      {
        id:'103',
        name: 'Infinix Smart 8',
        description: 'Infinix Smart 8 HD (64 GB) 3GB RAM (Crystal Green)',
        price: 29.99,
        image: 'https://m.media-amazon.com/images/I/210v671okQL._AC_UL424_FMwebp_QL65_.jpg'
      },
      {
        id:'103',
        name: 'Samsung TV',
        description: 'Samsung 80 cm (32 Inches) Wondertainment Series HD Ready LED Smart TV UA32T4340BKXXL (Glossy Black)',
        price: 29.99,
        image: 'https://m.media-amazon.com/images/I/71vSTDYCV8L._SX355_.jpg'
      },
      {
        id:'104',
        name: 'Apple iPad',
        description: 'Apple iPad (10th Generation): with A14 Bionic chip, 27.69 cm (10.9″) Liquid Retina Display, 256GB, Wi-Fi 6, 12MP front/12MP Back Camera, Touch ID, All-Day Battery Life – Pink',
        price: 29.99,
        image: 'https://m.media-amazon.com/images/I/61hZrbHRMWL._AC_UY218_.jpg'
      },
      {
        id:'105',
        name: 'boAt Airdopes',
        description: 'boAt Airdopes Atom 81 TWS Earbuds with Upto 50H Playtime, Quad Mics ENx™ Tech, 13MM Drivers,Super Low Latency(50ms), ASAP™ Charge, BT v5.3(Opal Black)',
        price: 29.99,
        image: 'https://m.media-amazon.com/images/I/61yyQD1KLOL._SY355_.jpg'
      },
      
    ];
  }

  

  ngOnInit() {
    
   
  }
  addToCart(product :any){
    this.service.addToCart(product);
  }
}